// File 1

var rows = 7,
    cols = 7,
    coefficient = 38,
    unicodeSymbols = ["◊", "✚", "♥", "▲"],
    sidesToCheck = [
        [0, -1],
        [0, 1],
        [-1, 0],
        [1, 0]
    ],
    style = new PIXI.TextStyle({
        fill: "#ff2b2b",
        fontFamily: "Helvetica",
        fontSize: 26,
        fontWeight: 700
    });

var app = new PIXI.Application({
    width: cols * coefficient,
    height: rows * coefficient,
    backgroundColor: 0xFFFFFF
});

document.body.appendChild(app.view);

function createField(rows, cols) {
    var result = [];
    for (var i = 0; i < rows; i++) {
        var tempRow = [];

        for (var j = 0; j < cols; j++) {

            var pixiTextObject = getRandomUnicode({
                x: j * (app.view.width / cols),
                y: i * (app.view.height / rows),
                interactive: true,
                buttonMode: true,
                scale: { x: 1, y: 1 },
                matrixPosition: { row: i, col: j },
            });

            pixiTextObject.on("pointerdown", unicodePointerDown);
            tempRow.push(pixiTextObject);
            app.stage.addChild(pixiTextObject);
        }
        result.push(tempRow);
    }
    return result;
}

function getRandomUnicode(options) {
    var intNumber = getRandomValue(0, unicodeSymbols.length),
        randomSymbol = unicodeSymbols[intNumber],
        text = new PIXI.Text(randomSymbol, options.textStyle);

    return Object.assign(text, options, { groupId: intNumber });
}

function getRandomValue(from, to) {
    return Math.floor(Math.random() * (to - from) + from);
}

