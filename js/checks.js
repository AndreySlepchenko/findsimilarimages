// File 2

function checkSameAround(startPoint) {
    var pointsToCheck = [startPoint],
        groupedElements = [startPoint];
    do {
        pointsToCheck.forEach(function (current) {
            var { row, col } = current.matrixPosition;
            for (var item of sidesToCheck) {
                var temp = { row: row + item[0], col: col + item[1] };

                if (!isCellExist(temp)) {
                    continue;
                }

                var toCheck = cellsMatrix[temp.row][temp.col];
                if (!isElemAlreadyAdded(toCheck, groupedElements) && isElemInSameGroup(current, toCheck)) {
                    pointsToCheck.push(toCheck);
                    groupedElements.push(toCheck);
                }
            }
        });
        pointsToCheck.shift();
    } while (pointsToCheck.length !== 0);
    return groupedElements;
}

function isCellExist(coord) {
    try {
        return cellsMatrix[coord.row][coord.col] !== undefined;
    } catch (error) {
        return false;
    }
}

function isElemInSameGroup(objA, objB) {
    if (!objA || !objB) {
        console.log('isElem error elem1 or 2');
        return false;
    }
    return objA.groupId === objB.groupId;
}

function isElemAlreadyAdded(elem, array) {
    return array.indexOf(elem) !== -1;
}

